# Makefile to compile cif2hkl.
# required: gfortran
#
# just type: make

# simple one-shot compile
all:	
	${FC} -ffree-line-length-512 \
	CFML/CFML_GlobalDeps_Linux.f90 CFML/CFML_Math_General.f90 \
	CFML/CFML_String_Utilities_gf.f90   CFML/CFML_Math_3D.f90 \
	CFML/CFML_Symmetry_Tables.f90        CFML/CFML_Scattering_Chemical_Tables.f90 \
	CFML/CFML_Crystallographic_Symmetry.f90         CFML/CFML_Crystal_Metrics.f90 \
	CFML/CFML_Reflections_Utilities.f90      CFML/CFML_Atom_TypeDef.f90 \
	CFML/CFML_Geometry_Calc.f90        CFML/CFML_Molecular_Crystals.f90 \
	CFML/CFML_Diffraction_Patterns.f90         CFML/CFML_Magnetic_Groups.f90 \
	CFML/CFML_EisPack.f90          CFML/CFML_IO_Formats.f90 \
	CFML/CFML_Structure_Factors.f90 -o cif2hkl cif2hkl.F90 -lm
	rm *.mod

clean: 
	rm -f *.o *.mod cif2hkl

install:
	install -D cif2hkl \
		$(DESTDIR)$(prefix)/usr/bin/cif2hkl

distclean: clean

uninstall:
	-rm -f $(DESTDIR)$(prefix)/usr/bin/cif2hkl

test:
	./cif2hkl --version
	./cif2hkl --verbose --xtal --no-outout-files examples/CaF2.cfl
	@echo Test OK

.PHONY: all install clean distclean uninstall test

